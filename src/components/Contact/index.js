import React from 'react'

export default function Contact() {
  return (
    <section className='contact adjust' id='contact'>
      <div className='title'>
        <h2>Interested in what you see</h2>
        <p>Don't be shy to contact me!</p>
      </div>
      <div className='contactForm'>
        <div className='row'>
          <input type='text' name='' placeholder='First Name' />
          <input type='text' name='' placeholder='Last Name' />
        </div>
        <div className='row'>
          <input type='text' name='' placeholder='Email Address' />
          <input type='text' name='' placeholder='Mobile No' />
        </div>
        <div className='row2'>
          <textarea placeholder='Message'></textarea>
        </div>
        <div className='row2'>
          <input type='submit' value='Send' />
        </div>
      </div>
    </section>
  )
}
