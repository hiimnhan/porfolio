import { StaticImage } from 'gatsby-plugin-image'
import React from 'react'

export default function About() {
  return (
    <section className='about adjust' id='about'>
      <div className='title'>
        <h2>About Me</h2>
      </div>
      <div className='about-content'>
        <div className='about-textBox'>
          <p>
            Fugiat deserunt minim Lorem officia amet anim cupidatat qui tempor
            proident voluptate sit quis eu. Anim eiusmod et commodo labore sunt
            elit enim ut cillum non proident. Nisi fugiat amet dolor sunt duis
            cillum. Ea ea amet sunt ullamco. Veniam eu amet est adipisicing et
            tempor.
            <br />
            <br />
            Tempor sunt cillum cillum magna eiusmod fugiat commodo quis sunt
            proident quis pariatur proident. Officia commodo magna voluptate
            magna commodo anim ea dolore. Sit eu pariatur do reprehenderit sint
            elit ullamco nostrud mollit do ut. Culpa aute duis esse culpa culpa
            laborum. Dolor officia minim officia pariatur veniam tempor tempor
            minim voluptate nisi sunt est cupidatat. Velit excepteur aliquip ex
            officia elit et aute ad ut fugiat tempor proident non incididunt.
          </p>
        </div>
        <div className='about-imgBx'>
          <StaticImage src='../../images/bg.JPG' className='bg' alt='bg' />
        </div>
      </div>
    </section>
  )
}
