import { Link } from 'gatsby'
import React from 'react'

export default function SocialList() {
  return (
    <ul className='social-list'>
      <li className='social-item'>
        <Link
          to='https://www.facebook.com/himnhann/'
          target='_blank'
          className='social-link'
        >
          <ion-icon name='logo-facebook'></ion-icon>
        </Link>
      </li>
      <li className='social-item'>
        <Link
          to='https://github.com/hiimnhan'
          target='_blank'
          className='social-link'
        >
          <ion-icon name='logo-github'></ion-icon>
        </Link>
      </li>
      <li className='social-item'>
        <Link
          to='https://www.linkedin.com/in/nhanncv/'
          target='_blank'
          className='social-link'
        >
          <ion-icon name='logo-linkedin'></ion-icon>
        </Link>
      </li>
      <li className='social-item'>
        <Link
          to='https://www.instagram.com/_vinhnhan/'
          target='_blank'
          className='social-link'
        >
          <ion-icon name='logo-instagram'></ion-icon>
        </Link>
      </li>
    </ul>
  )
}
