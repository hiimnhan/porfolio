import { StaticImage } from 'gatsby-plugin-image'
import React from 'react'
import SocialList from '../SocialList'

export default function Hero() {
  return (
    <section className='banner' id='banner'>
      <div className='content'>
        <div className='imgBx'>
          <StaticImage
            src='../../images/ava.JPG'
            className='avatar'
            alt='ava'
          />
        </div>
        <h3>Nguyen Chi Vinh Nhan</h3>
        <p>I'm a Frontend Developer </p>
        <p>And a fulltime lazy cat</p>
        <a href='#' className='btn'>
          My CV
        </a>
        <SocialList />
      </div>
    </section>
  )
}
