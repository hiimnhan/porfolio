import React, { useState } from 'react'
import { Helmet } from 'react-helmet'
import '../../styles/index.css'
import favicon from '../../images/favicon.ico'

import { StaticImage } from 'gatsby-plugin-image'
import { Link } from 'gatsby'
import Copyright from '../Copyright'
export default function Layout({ children }) {
  const [toggleMenu, setIsToggleMenu] = useState(false)

  const onToggleMenu = () => {
    setIsToggleMenu(toggleMenu => !toggleMenu)
  }
  return (
    <>
      <Helmet>
        <meta charSet='utf-8' />
        <title>Vinh Nhan Porfolio</title>
        <link rel='icon' href={favicon} />
        <script
          type='module'
          src='https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js'
        ></script>
        <script
          nomodule
          src='https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js'
        ></script>
      </Helmet>
      <div className='container'>
        <div className={`navigation ${toggleMenu ? 'active' : ''}`}>
          <ul className='navigation-list'>
            <li className='navigation-item' onClick={onToggleMenu}>
              <a className='navigation-item-link' href='#banner'>
                Home
              </a>
            </li>
            <li className='navigation-item' onClick={onToggleMenu}>
              <a className='navigation-item-link' href='#about'>
                About
              </a>
            </li>
            <li className='navigation-item' onClick={onToggleMenu}>
              <a className='navigation-item-link' href='#project'>
                Projects
              </a>
            </li>
            <li className='navigation-item' onClick={onToggleMenu}>
              <a className='navigation-item-link' href='#contact'>
                Contact
              </a>
            </li>
          </ul>
        </div>
        <div className={`main ${toggleMenu ? 'active' : ''}`}>
          <div className={`topbar ${toggleMenu ? 'active' : ''}`}>
            <Link to='/'>
              <div className='logo-wrapper'>
                <StaticImage
                  src='../../images/gatsby-icon.png'
                  className='logo'
                  alt='logo'
                />
              </div>
            </Link>
            <div
              className={`toggle ${toggleMenu ? 'active' : ''}`}
              onClick={onToggleMenu}
            >
              <ion-icon name='menu-outline'></ion-icon>
            </div>
          </div>
          {children}
          <Copyright />
        </div>
      </div>
    </>
  )
}
